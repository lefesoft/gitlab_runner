# Gitlab Docker Runner
This project configures a gitlab runner

## Setup
> **If on Windows, it is suggested to run these commands in wsl**

An existing config file is listed in `./config/config.toml`. You can adjust the concurrent limit to limit how many jobs run on the machine at once.

Start the docker-compose file. This will allow you to register runners.

To register a new runner, 
* insert your CI token from your group or repository into the script at `sh ./register_new_runner.sh`. 
  * Add a name in the description field. 
* Run the docker compose file
* execute the sh script. 

You should now see that your `config.toml` has been updated with a new runner.

To verify it worked successfully, go to your project and navigate to Settings >> CI/CD >> Runners, and you should see your description listed